﻿public abstract class Expression
{
    public abstract object Evaluate();
    public abstract override string ToString();
}
