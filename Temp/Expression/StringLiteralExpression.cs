﻿public class StringLiteralExpression : Expression
{
    public string value;

    public StringLiteralExpression(Token token)
    {
        value = token.literal;
    }

    public override object Evaluate()
    {
        return value.Substring(1, value.Length - 2);
    }

    public override string ToString() => value;
}
