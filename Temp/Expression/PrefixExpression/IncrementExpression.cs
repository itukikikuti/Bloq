﻿public class IncrementExpression : PrefixExpression
{
    public override object Evaluate()
    {
        if (right is IdentifierExpression identifierExpression)
        {
            if (Memory.variables[identifierExpression.identifier] is int value0)
            {
                Memory.variables[identifierExpression.identifier] = ++value0;
            }
        }

        object temp = right.Evaluate();

        if (temp is int value1)
            return ++value1;

        throw new System.Exception();
    }

    public override string ToString() => $"++ {right}";
}
