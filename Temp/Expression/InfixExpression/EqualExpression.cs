﻿public class EqualExpression : InfixExpression
{
    public override object Evaluate()
    {
        if (left.Evaluate() is int leftInt &&
            right.Evaluate() is int rightInt)
        {
            return leftInt == rightInt;
        }

        throw new System.Exception();
    }

    public override string ToString() => $"{left} == {right}";
}
