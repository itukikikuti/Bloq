﻿public abstract class InfixExpression : Expression
{
    public Expression left = null;
    public Expression right = null;
}
