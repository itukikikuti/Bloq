﻿public class IntLiteralExpression : Expression
{
    public int value;

    public IntLiteralExpression(Token token)
    {
        value = int.Parse(token.literal);
    }

    public override object Evaluate()
    {
        return value;
    }

    public override string ToString() => value.ToString();
}
