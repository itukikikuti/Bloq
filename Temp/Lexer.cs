﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class Lexer
{
    Queue<string> literals = null;

    (TokenType tokenType, Func<string, bool> isMatch)[] tokenPatterns = new (TokenType, Func<string, bool>)[]
    {
        (TokenType.EOF,             s => s == ""),
        (TokenType.NewLine,         s => s == "\\n"),
        (TokenType.Assign,          s => s == "="),
        (TokenType.Percent,         s => s == "%"),
        (TokenType.Equal,           s => s == "=="),
        (TokenType.Less,            s => s == "<"),
        (TokenType.Increment,       s => s == "++"),
        (TokenType.If,              s => s == "if"),
        (TokenType.Else,            s => s == "else"),
        (TokenType.Elseif,          s => s == "elseif"),
        (TokenType.While,           s => s == "while"),
        (TokenType.End,             s => s == "end"),
        (TokenType.Int,             s => s == "int"),
        (TokenType.IntLiteral,      s => Regex.IsMatch(s, @"^[0-9]+$")),
        (TokenType.StringLiteral,   s => Regex.IsMatch(s, @"^"".+""$")),
        (TokenType.Print,           s => s == "print"),
        (TokenType.Identifier,      s => Regex.IsMatch(s, @"^([a-z]|_)+$")),
    };

    public Lexer(string code)
    {
        var temp = Split(code + "\n");
        literals = new Queue<string>(temp);
        Debug.Log("[" + string.Join("]\n[", literals) + "]");
    }

    public Token Next()
    {
        if (literals.Count == 0)
            return null;

        var literal = literals.Peek();

        foreach (var tokenPattern in tokenPatterns)
        {
            if (tokenPattern.isMatch(literal))
            {
                literals.Dequeue();
                return new Token(tokenPattern.tokenType, literal);
            }
        }

        return null;
    }

    private string[] Split(string code)
    {
        // 改行を可視化する(下でホワイトスペースで分割するので前後にスペースを挿入)
        code = Regex.Replace(code, @"(\r\n|\n|\r)", " \\n ");
        Debug.Log(code);
        // ホワイトスペースで分割する
        return Regex.Split(code, @"\s+");
    }
}
