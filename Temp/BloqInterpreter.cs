﻿using UnityEngine;

public class BloqInterpreter : MonoBehaviour
{
    [SerializeField] private TextAsset code = null;

    private void Start()
    {
        var lexer = new Lexer(code.text);
        var parser = new Parser(lexer);
        var root = parser.Parse();
        Debug.Log(root.ToString());
        //Debug.Log(root.ToString().Replace("\n", "\\n\n"));
        root.Evaluate();
    }
}
