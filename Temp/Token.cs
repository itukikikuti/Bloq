﻿public enum TokenType
{
    EOF,
    NewLine,
    Assign,
    Percent,
    Equal,
    Less,
    Increment,
    If,
    Else,
    Elseif,
    While,
    End,
    Int,
    IntLiteral,
    StringLiteral,
    Print,
    Identifier,
}

public class Token
{
    public TokenType type;
    public string literal;

    public Token(TokenType type, string literal)
    {
        this.type = type;
        this.literal = literal;
    }

    public override string ToString()
    {
        return literal;
    }
}
