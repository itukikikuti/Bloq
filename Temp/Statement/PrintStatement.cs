﻿public class PrintStatement : Statement
{
    public Expression expression = null;

    public override void Evaluate()
    {
        UnityEngine.Debug.Log(expression.Evaluate());
    }

    public override string ToString() => $"print {expression}";
}
