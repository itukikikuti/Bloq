﻿using System.Collections.Generic;

public class BlockStatement : Statement
{
    public List<Statement> statements = new List<Statement>();

    public override void Evaluate()
    {
        foreach (var statement in statements)
        {
            statement.Evaluate();
        }
    }

    public override string ToString() => string.Join("\n", statements);
}
