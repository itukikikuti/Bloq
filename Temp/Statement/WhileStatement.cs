﻿public class WhileStatement : Statement
{
    public Expression condition = null;
    public BlockStatement body = null;

    public override void Evaluate()
    {
        while (condition.Evaluate() is bool conditionBool && conditionBool)
        {
            body.Evaluate();
        }
    }

    public override string ToString() => $"while {condition}\n{body}\nend";
}
