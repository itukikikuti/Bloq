﻿public abstract class Statement
{
    public abstract void Evaluate();
    public abstract override string ToString();
}
