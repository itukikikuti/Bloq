﻿using System.Collections.Generic;

public class IfStatement : Statement
{
    public class Elseif
    {
        public Expression condition = null;
        public BlockStatement body = null;
    }

    public Expression condition = null;
    public BlockStatement body = null;
    public List<Elseif> elseifs = new List<Elseif>();
    public BlockStatement elseBody = null;

    public override void Evaluate()
    {
        if (condition.Evaluate() is bool conditionBool0 && conditionBool0)
        {
            body.Evaluate();
        }
        else
        {
            foreach(var elseif in elseifs)
            {
                if (elseif.condition.Evaluate() is bool conditionBool1 && conditionBool1)
                {
                    elseif.body.Evaluate();
                    return;
                }
            }

            elseBody.Evaluate();
        }
    }

    public override string ToString()
    {
        var code = $"if {condition}\n{body}\n";

        foreach (var elseIf in elseifs)
        {
            code += $"elseif {elseIf.condition}\n{elseIf.body}\n";
        }

        if (elseBody != null)
        {
            code += $"else\n{elseBody}\n";
        }

        code += "end";
        return code;
    }
}
