﻿public class ExpressionStatement : Statement
{
    public Expression expression = null;

    public override void Evaluate() => expression.Evaluate();

    public override string ToString() => expression.ToString();
}
