﻿using System.Linq;

public enum Priority
{
    Prefix = 4,
    MulDivMod = 3,
    LessGreater = 2,
    Equals = 1,
    Lowest = 0,
}

public class Parser
{
    Lexer lexer = null;
    Token currentToken = null;
    Token nextToken = null;

    public Parser(Lexer lexer)
    {
        this.lexer = lexer;

        currentToken = lexer.Next();
        nextToken = lexer.Next();
    }

    private void ReadToken()
    {
        currentToken = nextToken;
        nextToken = lexer.Next();
    }

    private bool TryPeek(TokenType type)
    {
        if (nextToken.type == type)
        {
            ReadToken();
            return true;
        }

        return false;
    }

    public RootStatement Parse()
    {
        var root = new RootStatement();

        while (currentToken.type != TokenType.EOF)
        {
            var statement = ParseStatement();

            if (statement != null)
                root.statements.Add(statement);

            ReadToken();
        }

        return root;
    }

    private Statement ParseStatement()
    {
        switch (currentToken.type)
        {
            case TokenType.If:
                return ParseIfStatement();
            case TokenType.While:
                return ParseWhileStatement();
            case TokenType.Print:
                return ParsePrintStatement();
            case TokenType.Int:
                return null;
            default:
                return ParseExpressionStatement();
        }
    }

    private Expression ParseExpression(Priority priority)
    {
        Expression expression = null;

        switch (currentToken.type)
        {
            case TokenType.Increment:
                IncrementExpression incrementExpression = new IncrementExpression();
                ReadToken();
                incrementExpression.right = ParseExpression(Priority.Prefix);

                expression = incrementExpression;
                break;
            case TokenType.IntLiteral:
                expression = new IntLiteralExpression(currentToken);
                break;
            case TokenType.StringLiteral:
                expression = new StringLiteralExpression(currentToken);
                break;
            case TokenType.Identifier:
                expression = new IdentifierExpression(currentToken);
                break;
        }

        while (nextToken.type != TokenType.NewLine && priority < GetInfixPriority(nextToken))
        {
            ReadToken();
            expression = ParseInfixExpression(expression);
        }

        return expression;
    }

    private IfStatement ParseIfStatement()
    {
        var ifStatement = new IfStatement();

        ReadToken();
        ifStatement.condition = ParseExpression(Priority.Lowest);
        ReadToken();
        ifStatement.body = ParseBlockStatement(TokenType.Elseif, TokenType.Else, TokenType.End);

        while (currentToken.type == TokenType.Elseif)
        {
            var elseIf = new IfStatement.Elseif();

            ReadToken();
            elseIf.condition = ParseExpression(Priority.Lowest);
            ReadToken();
            elseIf.body = ParseBlockStatement(TokenType.Elseif, TokenType.Else, TokenType.End);

            ifStatement.elseifs.Add(elseIf);
        }

        if (currentToken.type == TokenType.Else)
        {
            ReadToken();
            ifStatement.elseBody = ParseBlockStatement(TokenType.End);
        }

        ReadToken();

        return ifStatement;
    }

    private WhileStatement ParseWhileStatement()
    {
        var whileStatement = new WhileStatement();

        ReadToken();
        whileStatement.condition = ParseExpression(Priority.Lowest);
        ReadToken();
        whileStatement.body = ParseBlockStatement(TokenType.End);
        ReadToken();

        return whileStatement;
    }

    private PrintStatement ParsePrintStatement()
    {
        var printStatement = new PrintStatement();

        ReadToken();
        printStatement.expression = ParseExpression(Priority.Lowest);
        ReadToken();

        return printStatement;
    }

    private BlockStatement ParseBlockStatement(params TokenType[] endTokenTypes)
    {
        var blockStatement = new BlockStatement();
        ReadToken();

        while (!endTokenTypes.Contains(currentToken.type) && currentToken.type != TokenType.EOF)
        {
            var statement = ParseStatement();
            if (statement != null)
                blockStatement.statements.Add(statement);

            ReadToken();
        }

        return blockStatement;
    }

    private ExpressionStatement ParseExpressionStatement()
    {
        var expressionStatement = new ExpressionStatement();
        expressionStatement.expression = ParseExpression(Priority.Lowest);
        ReadToken();

        return expressionStatement;
    }

    private Priority GetInfixPriority(Token token)
    {
        switch (token.type)
        {
            case TokenType.Percent:
                return Priority.MulDivMod;
            case TokenType.Equal:
                return Priority.Equals;
            case TokenType.Less:
                return Priority.LessGreater;
            default:
                return Priority.Lowest;
        }
    }

    private InfixExpression ParseInfixExpression(Expression left)
    {
        InfixExpression expression = null;

        switch (currentToken.type)
        {
            case TokenType.Percent:
                expression = new ModExpression();
                break;
            case TokenType.Equal:
                expression = new EqualExpression();
                break;
            case TokenType.Less:
                expression = new LessExpression();
                break;
        }

        expression.left = left;

        Priority priority = GetInfixPriority(currentToken);
        ReadToken();
        expression.right = ParseExpression(priority);

        return expression;
    }
}
