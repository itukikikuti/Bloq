﻿namespace Bloq
{
    public class IncrementExpression : PrefixExpression
    {
        public IncrementExpression(Interpreter interpreter)
        {
            interpreter.MoveNext();
            right = Expression.Parse(interpreter, InfixExpression.Priority.Prefix);
        }

        public override object Evaluate()
        {
            if (right is IdentifierExpression identifierExpression)
            {
                if (Memory.variables[identifierExpression.identifier] is int value0)
                {
                    Memory.variables[identifierExpression.identifier] = ++value0;
                }
            }

            object temp = right.Evaluate();

            if (temp is int value1)
                return ++value1;

            throw new IllegalExpressionTypeError();
        }

        public override string ToString() => $"++ {right}";
    }
}
