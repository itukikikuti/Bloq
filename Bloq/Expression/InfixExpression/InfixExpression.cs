﻿using System;

namespace Bloq
{
    public abstract class InfixExpression : Expression
    {
        public enum Priority
        {
            Prefix = 4,
            MulDivMod = 3,
            LessGreater = 2,
            Equals = 1,
            Lowest = 0,
        }

        public Expression left = null;
        public Expression right = null;

        internal static InfixExpression Parse(Interpreter interpreter, Expression left)
        {
            InfixExpression expression = null;

            switch (interpreter.Current.type)
            {
                case TokenType.Percent:
                    expression = new ModExpression(interpreter);
                    break;
                case TokenType.Equal:
                    expression = new EqualExpression(interpreter);
                    break;
                case TokenType.Less:
                    expression = new LessExpression(interpreter);
                    break;
            }

            expression.left = left;

            Priority priority = InfixExpression.TokenToPriority(interpreter.Current);
            interpreter.MoveNext();
            expression.right = Expression.Parse(interpreter, priority);

            return expression;
        }

        internal static Priority TokenToPriority(Token token)
        {
            switch (token.type)
            {
                case TokenType.Percent:
                    return Priority.MulDivMod;
                case TokenType.Equal:
                    return Priority.Equals;
                case TokenType.Less:
                    return Priority.LessGreater;
                default:
                    return Priority.Lowest;
            }
        }
    }
}
