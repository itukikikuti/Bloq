﻿namespace Bloq
{
    public class LessExpression : InfixExpression
    {
        public LessExpression(Interpreter interpreter)
        {
        }

        public override object Evaluate()
        {
            if (left.Evaluate() is int lvalue &&
                right.Evaluate() is int rvalue)
            {
                return lvalue < rvalue;
            }

            throw new IllegalExpressionTypeError();
        }

        public override string ToString() => $"{left} < {right}";
    }
}
