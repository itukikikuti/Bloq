﻿namespace Bloq
{
    public class ModExpression : InfixExpression
    {
        public ModExpression(Interpreter interpreter)
        {
        }

        public override object Evaluate()
        {
            if (left.Evaluate() is int lvalue &&
                right.Evaluate() is int rvalue)
            {
                return lvalue % rvalue;
            }

            throw new IllegalExpressionTypeError();
        }

        public override string ToString() => $"{left} % {right}";
    }
}
