﻿namespace Bloq
{
    public class IntLiteralExpression : Expression
    {
        public int value;

        public IntLiteralExpression(Interpreter interpreter)
        {
            value = int.Parse(interpreter.Current.literal);
        }

        public override object Evaluate()
        {
            return value;
        }

        public override string ToString() => value.ToString();
    }
}
