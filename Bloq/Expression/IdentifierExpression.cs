﻿namespace Bloq
{
    public class IdentifierExpression : Expression
    {
        public string identifier = null;

        public IdentifierExpression(Interpreter interpreter)
        {
            identifier = interpreter.Current.literal;
            Memory.variables[identifier] = 0;
        }

        public override object Evaluate()
        {
            return Memory.variables[identifier];
        }

        public override string ToString() => identifier;
    }
}
