﻿using System.Collections.Generic;

namespace Bloq
{
    public abstract class Expression
    {
        internal static Expression Parse(Interpreter interpreter, InfixExpression.Priority priority)
        {
            Expression expression = null;

            switch (interpreter.Current.type)
            {
                case TokenType.Increment:
                    expression = new IncrementExpression(interpreter);
                    break;
                case TokenType.IntLiteral:
                    expression = new IntLiteralExpression(interpreter);
                    break;
                case TokenType.StringLiteral:
                    expression = new StringLiteralExpression(interpreter);
                    break;
                case TokenType.Identifier:
                    expression = new IdentifierExpression(interpreter);
                    break;
            }

            while (interpreter.Next.type != TokenType.NewLine && priority < InfixExpression.TokenToPriority(interpreter.Next))
            {
                interpreter.MoveNext();
                expression = InfixExpression.Parse(interpreter, expression);
            }

            return expression;
        }

        public abstract object Evaluate();
        public abstract override string ToString();
    }
}
