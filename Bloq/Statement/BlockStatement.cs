﻿using System.Collections.Generic;
using System.Linq;

namespace Bloq
{
    public class BlockStatement : Statement
    {
        public List<Statement> statements = new List<Statement>();

        public BlockStatement(Interpreter interpreter, params TokenType[] endTokenTypes)
        {
            interpreter.MoveNext();

            while (!endTokenTypes.Contains(interpreter.Current.type) && interpreter.Current.type != TokenType.EOF)
            {
                var statement = Statement.Parse(interpreter);

                if (statement != null)
                    statements.Add(statement);

                interpreter.MoveNext();
            }
        }

        public override void Evaluate()
        {
            foreach (var statement in statements)
            {
                statement.Evaluate();
            }
        }

        public override string ToString() => string.Join("\n", statements);
    }
}
