﻿using System.Collections.Generic;

namespace Bloq
{
    public class RootStatement : Statement
    {
        public List<Statement> statements = new List<Statement>();

        public RootStatement(Interpreter interpreter)
        {
            while (interpreter.Current.type != TokenType.EOF)
            {
                var statement = Statement.Parse(interpreter);

                if (statement != null)
                    statements.Add(statement);

                interpreter.MoveNext();
            }
        }

        public override void Evaluate()
        {
            foreach (var statement in statements)
            {
                statement.Evaluate();
            }
        }

        public override string ToString() => string.Join("\n", statements);
    }
}
