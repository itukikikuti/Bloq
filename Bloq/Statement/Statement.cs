﻿namespace Bloq
{
    public abstract class Statement
    {
        internal static Statement Parse(Interpreter interpreter)
        {
            switch (interpreter.Current.type)
            {
                case TokenType.If:
                    return new IfStatement(interpreter);
                case TokenType.While:
                    return new WhileStatement(interpreter);
                case TokenType.Print:
                    return new PrintStatement(interpreter);
                case TokenType.Int:
                    return new IntStatement(interpreter);
                default:
                    return new ExpressionStatement(interpreter);
            }
        }

        public abstract void Evaluate();
        public abstract override string ToString();
    }
}
