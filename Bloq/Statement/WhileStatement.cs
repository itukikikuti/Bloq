﻿using System.Collections.Generic;

namespace Bloq
{
    public class WhileStatement : Statement
    {
        public Expression condition = null;
        public BlockStatement body = null;

        public WhileStatement(Interpreter interpreter)
        {
            interpreter.MoveNext();
            condition = Expression.Parse(interpreter, InfixExpression.Priority.Lowest);
            interpreter.MoveNext();
            body = new BlockStatement(interpreter, TokenType.End);
            interpreter.MoveNext();
        }

        public override void Evaluate()
        {
            while (condition.Evaluate() is bool value)
            {
                if (value)
                {
                    body.Evaluate();
                }
                else
                {
                    return;
                }
            }

            throw new IllegalExpressionTypeError();
        }

        public override string ToString() => $"while {condition}\n{body}\nend";
    }
}
