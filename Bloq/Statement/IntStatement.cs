﻿namespace Bloq
{
    public class IntStatement : Statement
    {
        public string identifier = null;
        public Expression expression = null;

        public IntStatement(Interpreter interpreter)
        {
            interpreter.MoveNext();
            identifier = interpreter.Current.literal;
            interpreter.MoveNext();
            interpreter.MoveNext();
            expression = Expression.Parse(interpreter, InfixExpression.Priority.Lowest);
            interpreter.MoveNext();
        }

        public override void Evaluate()
        {
        }

        public override string ToString() => $"int {identifier} = {expression}";
    }
}
