﻿using System.Collections.Generic;

namespace Bloq
{
    public class ExpressionStatement : Statement
    {
        public Expression expression = null;

        public ExpressionStatement(Interpreter interpreter)
        {
            expression = Expression.Parse(interpreter, InfixExpression.Priority.Lowest);
            interpreter.MoveNext();
        }

        public override void Evaluate() => expression.Evaluate();

        public override string ToString() => expression.ToString();
    }
}
