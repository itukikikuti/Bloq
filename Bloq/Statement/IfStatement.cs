﻿using System.Collections.Generic;
using System.Linq;

namespace Bloq
{
    public class IfStatement : Statement
    {
        public List<(Expression condition, BlockStatement body)> pairs = new List<(Expression condition, BlockStatement then)>();
        public BlockStatement elseBody = null;

        public IfStatement(Interpreter interpreter)
        {
            interpreter.MoveNext();
            var condition = Expression.Parse(interpreter, InfixExpression.Priority.Lowest);
            interpreter.MoveNext();
            var body = new BlockStatement(interpreter, TokenType.Elseif, TokenType.Else, TokenType.End);

            pairs.Add((condition, body));

            while (interpreter.Current.type == TokenType.Elseif)
            {
                interpreter.MoveNext();
                condition = Expression.Parse(interpreter, InfixExpression.Priority.Lowest);
                interpreter.MoveNext();
                body = new BlockStatement(interpreter, TokenType.Elseif, TokenType.Else, TokenType.End);

                pairs.Add((condition, body));
            }

            if (interpreter.Current.type == TokenType.Else)
            {
                interpreter.MoveNext();
                elseBody = new BlockStatement(interpreter, TokenType.End);
            }

            interpreter.MoveNext();
        }

        public override void Evaluate()
        {
            foreach (var pair in pairs)
            {
                if (pair.condition.Evaluate() is bool value)
                {
                    if (value)
                    {
                        pair.body.Evaluate();
                        return;
                    }
                }
                else
                {
                    throw new IllegalExpressionTypeError();
                }
            }

            elseBody.Evaluate();
        }

        public override string ToString()
        {
            var code = $"if {pairs.First().condition}\n{pairs.First().body}\n";

            foreach (var pair in pairs.Skip(1))
            {
                code += $"elseif {pair.condition}\n{pair.body}\n";
            }

            if (elseBody != null)
            {
                code += $"else\n{elseBody}\n";
            }

            code += "end";
            return code;
        }
    }
}
