﻿using System;
using System.Collections.Generic;

namespace Bloq
{
    public class PrintStatement : Statement
    {
        public Expression expression = null;

        public PrintStatement(Interpreter interpreter)
        {
            interpreter.MoveNext();
            expression = Expression.Parse(interpreter, InfixExpression.Priority.Lowest);
            interpreter.MoveNext();
        }

        public override void Evaluate()
        {
            Console.WriteLine(expression.Evaluate());
        }

        public override string ToString() => $"print {expression}";
    }
}
