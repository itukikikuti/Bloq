﻿using System;

namespace Bloq
{
    class Error : Exception
    {
    }

    class IllegalTokenError : Error
    {
    }

    class IllegalExpressionTypeError : Error
    {
    }
}
