﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Bloq
{
    public class Interpreter
    {
        private (TokenType tokenType, string pattern)[] tokenPatterns = new (TokenType, string)[]
        {
            (TokenType.NewLine,         @"\\n"),
            (TokenType.Assign,          @"="),
            (TokenType.Percent,         @"%"),
            (TokenType.Equal,           @"=="),
            (TokenType.Less,            @"<"),
            (TokenType.Increment,       @"\+\+"),
            (TokenType.If,              @"if"),
            (TokenType.Else,            @"else"),
            (TokenType.Elseif,          @"elseif"),
            (TokenType.While,           @"while"),
            (TokenType.End,             @"end"),
            (TokenType.Int,             @"int"),
            (TokenType.IntLiteral,      @"[0-9]+"),
            (TokenType.StringLiteral,   @""".+"""),
            (TokenType.Print,           @"print"),
            (TokenType.Identifier,      @"([a-z]|_)+"),
        };

        private Token[] tokens = null;
        private int tokenIndex = 0;
        private RootStatement root = null;

        public void Tokenize(string code)
        {
            code += "\n";
            // 改行を可視化する(下でホワイトスペースで分割するので前後にスペースを挿入)
            code = Regex.Replace(code, @"(\r\n|\n|\r)+", " \\n ");

            // ホワイトスペースで分割する
            tokens = Regex.Split(code, @"\s+").Select(s => ToToken(s)).ToArray();

            System.Console.WriteLine("[" + string.Join("]\n[", tokens.AsEnumerable()) + "]");
        }

        public Token Current
        {
            get
            {
                if (tokens.Length > tokenIndex)
                    return tokens[tokenIndex];

                return new Token(TokenType.EOF, "");
            }
        }

        public Token Next
        {
            get
            {
                if (tokens.Length > tokenIndex + 1)
                    return tokens[tokenIndex + 1];

                return new Token(TokenType.EOF, "");
            }
        }

        public void MoveNext()
        {
            ++tokenIndex;
        }

        public void Parse()
        {
            root = new RootStatement(this);

            System.Console.WriteLine(root);
        }

        public void Evaluate()
        {
            root.Evaluate();
        }

        private Token ToToken(string literal)
        {
            if (string.IsNullOrEmpty(literal))
                return new Token(TokenType.EOF, "");

            foreach (var tokenPattern in tokenPatterns)
            {
                if (Regex.IsMatch(literal, $"^{tokenPattern.pattern}$"))
                {
                    return new Token(tokenPattern.tokenType, literal);
                }
            }

            throw new IllegalTokenError();
        }
    }
}