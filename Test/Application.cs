﻿using System.IO;
using Bloq;

namespace Test
{
    public class Application
    {
        private static void Main()
        {
            string code;
            using (StreamReader streamReader = new StreamReader("Test01.bq"))
            {
                code = streamReader.ReadToEnd();
            }

            Interpreter interpreter = new Interpreter();
            interpreter.Tokenize(code);
            interpreter.Parse();
            interpreter.Evaluate();
        }
    }
}
